import Vue from 'vue'
import VueRouter from 'vue-router'
import libs from '../libs/libs'
import routes from './routes'

Vue.use(VueRouter)

/*
* If not building with SSR mode, you can
* directly export the Router instantiation
*/

// export default function (/* { store, ssrContext } */) {
const Router = new VueRouter({
scrollBehavior: () => ({ y: 0 }),
routes,

// Leave these as is and change from quasar.conf.js instead!
// quasar.conf.js -> build -> vueRouterMode
// quasar.conf.js -> build -> publicPath
mode: process.env.VUE_ROUTER_MODE,
base: process.env.VUE_ROUTER_BASE
})

// Router.beforeEach((to, from, next) => {
// const publicPages = ['/']
// const authRequired = !publicPages.includes(to.path)
// const loggedIn = localStorage.getItem('11223344556677889900')

// if (authRequired && !loggedIn) {
// return next('/')
// }
// next()
// })
// return Router
// }

// to=a donde voy, from=desde donde voy, next=hacia donde voy
Router.beforeEach((to, from, next) => {
  // Objeto para definir a donde redirigir
  let nextObject = {}

  // Buscar el meta requiresAuth
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Validar que esté autenticado
    if (!libs.isAuthenticated()) {
      nextObject = {
        path: '/',
        query: { redirect: to.fullPath }
      }
    } else {
      // Buscar el meta permissionsCodeName
      if (to.matched.some(record => record.meta.permissionsCodeName)) {
        // Validar que tenga el permiso
        if (!libs.hasPermission(to.meta.permissionsCodeName)) {
          libs.createAlert('negative', 'No está autorizado para acceder a la ruta solicitada.', 'warning', 'top-right', 3000)
          nextObject = {
            path: '/'
          }
        }
      }
    }
  }
  next(nextObject)
})
export default Router
