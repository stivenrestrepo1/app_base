
const routes = [
  {
    path: '/',
    component: () => import('components/login/LoginComponent.vue')
  },
  {
    path: '/reset/password/:token/',
    component: () => import('components/ResetPassword/ResetPassword')
  },
  {
    path: '/login',
    component: () => import('components/login/LoginComponent.vue')
  },
  {
    path: '/home',
    component: () => import('components/Menu/MenuMain.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      { path: '/home', component: () => import('pages/Home.vue') },
      { path: '/formusers', component: () => import('components/User/FormUsers.vue') },
      { path: '/userlist', component: () => import('components/User/UsersList.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
