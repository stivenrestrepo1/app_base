import axios from 'axios'
import libs from '../libs/libs'
import { Notify } from 'quasar'
import { Loading, QSpinnerBall } from 'quasar'
import Vue from 'vue'

Vue.use({QSpinnerBall, Loading})
const axiosInstace = axios.create({
    // baseURL: 'http://192.168.56.101:8000/api/'
    baseURL: 'http://10.1.148.241:8001/api/'
    // baseURL: 'http://10.1.148.204:8008/api/'
})

// Interceptar request para enviar el encabezado de autorización
axiosInstace.interceptors.request.use(config => {
  // Enviar el token si existe en LocalStorage
  let token = localStorage.getItem('11223344556677889900')
  if (token !== null) {
    config['headers']['Authorization'] = 'JWT ' + token
  }
  return config
}, error => {
  return Promise.reject(error)
})

axiosInstace.interceptors.response.use(response => {
  return response
}, error => {
  // Preparar alertas de acuerdo a las respuestas de error recibidas
  let NotifyConfig = {
    position: 'top',
    type: 'negative',
    message: 'Ha occurido un error',
    timeout: 4000,
    actions: [
      {
        icon: 'cancel'
      }
    ]
  }
  let showNotify = false

  // Si error tiene respuesta
  if (error.response) {
    // Validar errores de formulario non_field_errors
    if (typeof error.response.data.non_field_errors !== 'undefined') {
      let html = ''
      error.response.data.non_field_errors.forEach(element => {
        html += 'Los datos no son correctos' + '\n'
      })
      NotifyConfig['icon'] = 'error'
      NotifyConfig['message'] = html
      showNotify = true
    // Validar error 401 (Normalmente el token expiró)
    } else if (error.response.status === 401) {
      NotifyConfig['icon'] = 'error'
      if (error.response.data.detail === 'Signature has expired.') {
        NotifyConfig['message'] = 'Su sesión expiró.'
        Notify.create(NotifyConfig)
        libs.logOut()
      } else {
        NotifyConfig['message'] = 'Ha ocurrido un error.'
        showNotify = true
      }
    // Validar errores diferentes a los campos de los formularios
    } else if (error.response.status !== 400) {
      NotifyConfig['icon'] = 'error'
      NotifyConfig['message'] = 'Ha ocurrido un error.'
      showNotify = true
    }
  // Validar si el backend no está respondiendo
  } else if (error.request) {
    NotifyConfig['icon'] = 'cloud_off'
    NotifyConfig['message'] = 'La aplicación no está respondiendo a las peticiones, por favor intente más tarde.'
    showNotify = true
  // Validar si no estamos haciendo bien las solicitudes
  } else {
    NotifyConfig['icon'] = 'cloud_outline'
    NotifyConfig['message'] = 'La aplicación no está realizando las peticiones correctamente, por favor intente más tarde.'
    showNotify = true
  }
  // Mostrar o nó la notificación
  if (showNotify) {
    Notify.create(NotifyConfig)
  }
  // Retornan la promesa
  return Promise.reject(error)
})

// Exportar axios como instancia en Vue
export default ({ Vue }) => {
  Vue.prototype.$axios = axiosInstace
}
