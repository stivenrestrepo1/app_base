import 'quasar-extras/animate/bounceInDown.css'
import 'quasar-extras/animate/bounceOutUp.css'
import { Notify, Dialog } from 'quasar'
import router from '../router'

const libs = {
  /**
   * Función para validar que el usuario se encuentre logueado
   */
  isAuthenticated () {
    return localStorage.getItem('11223344556677889900') !== null || false
  },
  /**
   * @param {String} color Color de la alerta
   * @param {String} message Contenido de la alerta
   * @param {String} icon Icono de la alerta
   * @param {String} position Posición en pantalla de la alerta
   * @param {String} timeout Tiempo que tarda en coultar la alerta (milisegundos)
   */
  createAlert (color, message, icon, position, timeout) {
    Notify.create({
      color: color,
      message: message,
      icon: icon,
      position: position,
      timeout: timeout
    })
  },
  /**
   * Función para desplegar una alerta que tenga la opción de confirmar
   * para luego realizar una redirección de página
   * @param {String} redirect
   * @param {String} message
   */
  confirmAlert (redirect, message) {
    Dialog.create({
      title: 'Confirmación',
      message: message,
      ok: 'Aceptar',
      cancel: 'Cancelar'
    }).then(() => {
      router.push(redirect)
    }).catch(() => {})
  },
  getUsername () {
    return localStorage.getItem('username')
  },
  /**
   * Función para capturar los permisos devueltos al momento de hacer el login
   * @param {Object} permissionList Lista de permisos contenidos en el localStorage
   */
  hasPermission (permissionList) {
    try {
      let permissionsLocalStorage = JSON.parse(localStorage.getItem('permissions'))
      let isSuperUser = (localStorage.getItem('is_superuser') === 'true')
      let havePermission = true
      permissionList.forEach(element => {
        if (permissionsLocalStorage.indexOf(element) < 0) {
          havePermission = false
        }
      })
      return (isSuperUser || havePermission)
    } catch (error) {
      console.log('No tienen permisos', error)
    }
  },
  clearData (obj, excludes) {
    if (!excludes) {
      excludes = []
    }

    Object.keys(obj).forEach(element => {
      if (excludes.indexOf(element) === -1) {
        obj[element] = ''
      }
    })
    return obj
  },
  logOut () {
    localStorage.clear()
    router.push('/login')
  }
}

export default libs
